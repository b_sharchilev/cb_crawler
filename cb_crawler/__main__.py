import argparse
import json
import pandas as pd
import logging
from datetime import datetime, date

from tqdm import tqdm
from multiprocessing import pool

from response_parser import request_permalink
from util import PickleableFactorizer

# TODO: logging
#logging.getLogger().addHandler(logging.StreamHandler())

def parse_command_line_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input',
        type=str,
        required=True,
        help='Path to csv with permalinks'
    )
    parser.add_argument(
        '--prev-input',
        type=str,
        default='',
        help='Path to previously collected csv in same format, if present. Can be used for only collecting updated companies'
    )
    parser.add_argument(
        '--force-update',
        action='store_true',
        help='Should we download even companies that have not been updated since last collection?'
    )
    parser.add_argument(
        '--output',
        type=str,
        required=True,
        help='Path to output json'
    )
    parser.add_argument(
        '--funding-rounds',
        action='store_true',
        help='Are we downloading funding rounds?'
    )
    parser.add_argument(
        '--value-column',
        type=str,
        default='permalink',
        help='Name of the column with permalinks to query at endpoint'
    )
    parser.add_argument(
        '--date-column',
        type=str,
        default='updated_at',
        help='Name of the columns with entity update dates.'
    )
    parser.add_argument(
        '--max-attempts',
        type=int,
        default=5,
        help='Maximum number of attempts done for a single URL'
    )
    parser.add_argument(
        '--sleep-seconds-on-ban',
        type=int,
        default=60,
        help='Time (in seconds) to sleep if CB responds with 401'
    )
    parser.add_argument(
        '--logfile',
        type=str,
        default='log.log',
        help='Path to log file'
    )
    parser.add_argument(
        '--retry-output',
        type=str,
        default='',
        help='Where should the failed entries be stored. Format will be the same as in the input file.'
    )
    parser.add_argument(
        '--max-news-pages',
        type=int,
        default=1000000,
        help='Maximum number of news pages saved for a company.'
    )
    parser.add_argument(
        '--num-proc',
        type=int,
        default=1,
        help='Number of processes for parallel download.'
    ) 
    return parser.parse_args()

def setup_logger(log_file_path):
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.INFO)
    fileHandler = logging.FileHandler(log_file_path)
    rootLogger.addHandler(fileHandler)
   
    #consoleHandler = logging.StreamHandler()
    #rootLogger.addHandler(consoleHandler)

def main():
    args = parse_command_line_arguments()
    setup_logger(args.logfile)

    input_table = pd.read_csv(args.input)
    if not args.force_update and len(args.prev_input) > 0:
        total_num_permalinks = len(input_table)

        prev_input = pd.read_csv(args.prev_input, header=None, sep='\t')
        prev_input.columns = [args.value_column, args.date_column]
        if args.funding_rounds:
            prev_input[args.value_column] = prev_input[args.value_column].apply(add_dashes_to_uuid)
        input_table = filter_new_permalinks(input_table, prev_input, args.value_column, args.date_column)
        logging.getLogger().info('Querying %s new/updated permalinks out of %s'%(len(input_table),total_num_permalinks)) 
    values = input_table[args.value_column]

    request_mapping = PickleableFactorizer(_request_result_and_row, 
                                           endpoint='funding-rounds' if args.funding_rounds else '',
                                           max_attempts=args.max_attempts,
                                           sleep_seconds_on_ban=args.sleep_seconds_on_ban,
                                           max_news_pages=args.max_news_pages)
    output_file = open(args.output, 'w')
    progress_bar = tqdm(total=len(values), leave=True, miniters=1)
    permalinks_processed = 0
    retry_rows = []
    p = pool.Pool(args.num_proc)
    output_file.write('[')
    for parsed_result, row in p.imap_unordered(request_mapping, zip(values, input_table.values)):
        if 'Error' in parsed_result.keys():
            if parsed_result['Error'].retry_flag:
                # Download failed non-fatally, add to retry list
                retry_rows.append(row)
            permalinks_processed += 1
            continue

        result_str = json.dumps(parsed_result)
        permalinks_processed += 1
        if permalinks_processed < len(values):
            result_str += ','
        output_file.writelines(result_str)
        progress_bar.update(1)

    output_file.write(']')
    output_file.close()
    progress_bar.close()

    # Finally, output entries that should be retried
    retry_table = pd.DataFrame(retry_rows, columns=input_table.columns)
    retry_table.to_csv(args.retry_output, index=False)

    print('Done!')

def _request_result_and_row(permalink_and_row, **kwargs):
    return request_permalink(permalink_and_row[0], **kwargs), permalink_and_row[1]

def filter_new_permalinks(new_input, old_input, value_column, date_column):
    new_permalinks_dates = permalink_date_dict(new_input[value_column], new_input[date_column])
    old_permalinks_dates = permalink_date_dict(old_input[value_column], old_input[date_column])
    permalinks_to_update = set()
    for new_permalink in new_permalinks_dates.keys():
        prev_update_date = old_permalinks_dates.get(new_permalink)
        if prev_update_date is None:
            permalinks_to_update.add(new_permalink)
        else:
            new_update_date = new_permalinks_dates[new_permalink]
            if new_update_date > prev_update_date:
                permalinks_to_update.add(new_permalink)
    return new_input.loc[[permalink in permalinks_to_update for permalink in new_input[value_column]], :]

def permalink_date_dict(permalinks, dates):
    date_format_full = '%Y-%m-%d %H:%M:%S.%f'
    date_format_short = '%Y-%m-%d %H:%M:%S'
    dates_parsed = []
    for permalink,date in zip(permalinks, dates):
        #print(permalink, date)
        try:
           date_parsed = datetime.strptime(date, date_format_full).date()
        except ValueError:
           date_parsed = datetime.strptime(date, date_format_short).date()
        dates_parsed.append(date_parsed)
    return {permalink:date_parsed for permalink, date_parsed in zip(permalinks, dates_parsed)}

def add_dashes_to_uuid(uuid):
    uuid_list = list(uuid)
    for dash_position in [8, 13, 18, 23]:
        uuid_list.insert(dash_position, '-')
    return ''.join(uuid_list)

if __name__ == '__main__':
    main()
