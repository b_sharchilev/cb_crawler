import requests
import time
import logging

logger = logging.getLogger('__main__')

HTTP_CODE_OK = 200
HTTP_CODE_BAN = 401
HTTP_CODE_NOPAGE = 404

def request_json_with_retry(url, max_attempts, sleep_seconds_on_ban):
    return _make_ith_attempt(url, 1, max_attempts, sleep_seconds_on_ban)

def _make_ith_attempt(url, i, max_attempts, sleep_seconds_on_ban, retry_flag=True):
    if i >= max_attempts:
        e = RuntimeError('Maximum retries done on %s with no success'%url)
        e.retry_flag = retry_flag
        raise e
    try:
        response = requests.get(url)
    except requests.exceptions.RequestException, e:
        # Log error (TODO) and try again
        logger.info(e)
        return _make_ith_attempt(url, i + 1, max_attempts, sleep_seconds_on_ban)
    if response.status_code == HTTP_CODE_OK:
        try:
            return response.json()
        except Exception, e:
            e.retry_flag=False
            raise e
    elif response.status_code == HTTP_CODE_NOPAGE:
        # 404, no retry
	return _make_ith_attempt(url, i + max_attempts, max_attempts, sleep_seconds_on_ban, retry_flag=False)
    elif response.status_code == HTTP_CODE_BAN:
        # Have to wait before retrying
        logger.info('[{}]Ban response on URL {}, attempt {} after {} sec'.format(
                          response.status_code,
                          url,
                          i+1,
                          sleep_seconds_on_ban))
        time.sleep(sleep_seconds_on_ban)
        return _make_ith_attempt(url, i + 1, max_attempts, sleep_seconds_on_ban)
    else:
        # TODO: check what types of HTTP codes are returned and should we retry/wait on them
        logger.info('[{}] Message {} on URL {}, attempt {}'.format(response.status_code,
                                                                   response.text,
                                                                   url,
                                                                   i+1))
        time.sleep(sleep_seconds_on_ban)
        return _make_ith_attempt(url, i + max_attempts, max_attempts, sleep_seconds_on_ban)

