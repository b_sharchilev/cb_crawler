class PickleableFactorizer:
    """
    Make a factory out of a function (e.g. if we need to call `foo(x, **kwargs)`, initialize
    `bar = PickleableFactorizer(foo, **kwargs)` and call `bar(x)`) to make it
        a) Pickleable,
        b) usable with `map`.
    They can also be composed.
    """
    def __init__(self, function, **kwargs):
        self.function = function
        self.kwargs = kwargs

    def __call__(self, x):
        return self.function(x, **self.kwargs)

    def __add__(self, other):
        class Adder(PickleableFactorizer):
            def __init__(self, first, second):
                self.first = first
                self.second = second
            def __call__(self, x):
                return self.first(self.second(x))
        return Adder(self, other)

