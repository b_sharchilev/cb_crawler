import logging
from retry_requester import request_json_with_retry
from endpoint_router import CBEndpointRouter

logger = logging.getLogger('__main__')

def request_permalink(permalink, endpoint, max_attempts, sleep_seconds_on_ban, max_news_pages):
    endpoint_url_mapper = CBEndpointRouter(endpoint)
    try:
        response = request_json_with_retry(endpoint_url_mapper.permalink_to_url(permalink),
                                           max_attempts,
                                           sleep_seconds_on_ban)
    except Exception, e: # TODO: proper error handling
        logger.info(e)
        return {'Error':e}

    output = response['data'].copy()
    for rel_key, rel_item in response['data']['relationships'].items():
        # Check if some items did not fit into single page
        if rel_item['paging']['total_items'] > 10:
            details_url = rel_item['paging']['first_page_url']
            #details_url_with_key = endpoint_url_mapper.append_user_key(details_url)
            try:
                new_item = _request_details(details_url, endpoint_url_mapper, max_attempts, sleep_seconds_on_ban, max_news_pages)
            except Exception, e:
                logger.info(e)
                return {'Error':e}
        elif rel_item['paging']['total_items'] == 0:
            new_item = []
        else:
            if rel_item['cardinality'] in ['OneToOne', 'ManyToOne']:
                new_item = rel_item['item']
            else:
                new_item = rel_item['items']
        output['relationships'][rel_key] = new_item
    return output

def _request_details(url, endpoint_router, max_attempts, sleep_seconds_on_ban, max_news_pages):
    output = []
    not_over = True
    count = 0
    next_page_url = url
    while not_over and count < max_news_pages:
        response = request_json_with_retry(endpoint_router.append_user_key(next_page_url),
                                           max_attempts, sleep_seconds_on_ban)['data']        
        new_item = response['items']
        output.extend(new_item)
        next_page_url = response['paging']['next_page_url']
        count += 1
        not_over = count < response['paging']['number_of_pages']
    return output
