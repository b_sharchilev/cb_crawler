from os.path import join

class CBEndpointRouter:
    """
    Given an endpoint and a user API key, translates a permalink to a URL to query via HTTP.
    """
    def __init__(self, endpoint, user_key='933f3a43b201f1ffc51d71669b4c2fda'):
        self.base_link = 'http://api.crunchbase.com/v/3'
        self.endpoint = endpoint
        self.user_key = user_key

    def permalink_to_url(self, permalink):
        url_without_key = join(self.base_link, self.endpoint, permalink[1:] if permalink.startswith('/') else permalink)
        #print self.base_link, self.endpoint, permalink, self.append_user_key(url_without_key)
        return self.append_user_key(url_without_key)

    def append_user_key(self, url):
        # Key may already be there, in which case no action is needed
        if 'user_key' in url:
            return url
        if '?' in url:
            escape_char = '&'
        else:
            escape_char = '?'
        return url + escape_char + 'user_key=' + self.user_key

